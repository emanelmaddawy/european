/* global src */

$(document).ready(function () {
	var scrollButton = $("#scroll-top");

  $(window).scroll(function () {
    if ($(this).scrollTop() >= 500) {
      scrollButton.show();
    } else {
      scrollButton.hide();
    }

  });
  scrollButton.click(function () {
    $("html,body").animate({
      scrollTop: 0
    }, 600);
  });
  
  new WOW().init();


  $('.owl-carousel#sync1').owlCarousel({
    items:1,
    loop:true,
    nav: true,
    navText: ['<i class="fa fa-angle-left"><i>', '<i class="fa fa-angle-right"><i>'],
    URLhashListener:true,
    autoplayHoverPause:true,
    startPosition: 'URLHash'
  });


  $('.owl-carousel#sync2').owlCarousel({
    items:5,
    loop:true,
    nav: true,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    autoplayHoverPause:true,
    startPosition: 'URLHash',
    responsive:{
      0:{
        items:1,
      },
      600:{
        items:3,
        mouseDrag:true
      },
      1000:{
        items:5
      }
    }
  });

  $('.part').magnificPopup({
    delegate: 'a',
    type: 'image',
    closeOnContentClick: false,
    closeBtnInside: false,
    mainClass: 'mfp-with-zoom mfp-img-mobile',
    image: {
      verticalFit: true,
      titleSrc: function(item) {
        return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
      }
    },
    gallery: {
      enabled: true
    },
    zoom: {
      enabled: true,
      duration: 300, // don't foget to change the duration also in CSS
      opener: function(element) {
        return element.find('img');
      }
    }
    
  });

});


